package com.eightam.android.toaster;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.eightam.android.drawer.ShowToasterSpecification;
import com.eightam.android.drawer.Toaster;
import com.eightam.android.drawer.ToasterContentProvider;
import com.eightam.android.drawer.ToasterController;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private ToasterController toasterController;
    private Toaster toaster;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toaster = (Toaster) findViewById(R.id.toaster);
        toasterController = new ToasterController(toaster);

        findViewById(R.id.button_trigger_definite).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ToasterContentProvider toasterContentProvider = new DefiniteToasterContentProvider();

                Toaster.ToasterAppearance toasterAppearance = new Toaster.ToasterAppearance();
                toasterAppearance.toasterBackgroundColor = 0xffff0000;

                ShowToasterSpecification showToasterSpecification = new ShowToasterSpecification.Builder()
                        .id(UUID.randomUUID().toString())
                        .toasterContentProvider(toasterContentProvider)
                        .animate(true)
                        .toasterAppearance(toasterAppearance)
                        .build();

                toasterController.addNewShowToasterMessage(showToasterSpecification, Toaster.SHORT_DURATION_MILLIS);
                toasterController.startMessageConsumption();
            }

        });

        findViewById(R.id.button_trigger_indefinite).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ToasterContentProvider toasterContentProvider = new IndefiniteToasterContentProvider(toasterController);

                Toaster.ToasterAppearance toasterAppearance = new Toaster.ToasterAppearance();
                toasterAppearance.toasterBackgroundColor = 0xff00ff00;

                ShowToasterSpecification showToasterSpecification = new ShowToasterSpecification.Builder()
                        .id(UUID.randomUUID().toString())
                        .toasterContentProvider(toasterContentProvider)
                        .animate(true)
                        .toasterAppearance(toasterAppearance)
                        .build();

                toasterController.addNewShowStickyToasterMessage(showToasterSpecification);
                toasterController.startMessageConsumption();
            }

        });

        findViewById(R.id.button_hide).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                toasterController.hideToaster(true);
            }

        });

        findViewById(R.id.button_info).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (toast == null) {
                    toast = Toast.makeText(getApplicationContext(),
                            "Toaster shown: " + toaster.isToasterShown()
                                    + ", isShowing: " + toaster.isToasterShowing()
                                    + ", isHiding: " + toaster.isToasterHiding(), Toast.LENGTH_SHORT);
                } else {
                    toast.setText("Toaster shown: " + toaster.isToasterShown()
                            + ", isShowing: " + toaster.isToasterShowing()
                            + ", isHiding: " + toaster.isToasterHiding());

                    toast.setDuration(Toast.LENGTH_SHORT);
                }

                toast.show();
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        toasterController.startMessageConsumption();
    }

    @Override
    protected void onPause() {
        super.onPause();
        toasterController.stopMessageConsumption();
    }

    static String getRandomString() {
        int wordCount = (int) (Math.random() * 10) + 5;
        int characterCount = (int) (Math.random() * 10) + 5;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < wordCount; i++) {
            sb.append(RandomStringUtils.random(characterCount, true, false));

            if (i < wordCount - 1) {
                sb.append(" ");
            } else {
                sb.append(".");
            }
        }

        return sb.toString();
    }

    static class DefiniteToasterContentProvider implements ToasterContentProvider {

        @Override
        @NonNull
        public View provideToasterContentView(Context context, ViewGroup parent) {
            View contentView = LayoutInflater.from(context).inflate(R.layout.view_simple_message, parent, false);

            ((TextView) contentView.findViewById(R.id.text_message)).setText("[" + System.currentTimeMillis() + "] This is a definite-duration message. " + getRandomString());

            return contentView;
        }

    }

    static class IndefiniteToasterContentProvider implements ToasterContentProvider {

        ToasterController toasterController;

        public IndefiniteToasterContentProvider(ToasterController toasterController) {
            this.toasterController = toasterController;
        }

        @Override
        @NonNull
        public View provideToasterContentView(Context context, ViewGroup parent) {
            View contentView = LayoutInflater.from(context).inflate(R.layout.view_complex_message, parent, false);

            ((TextView) contentView.findViewById(R.id.text_title)).setText("The Title of the Message");
            ((TextView) contentView.findViewById(R.id.text_message)).setText("[" + System.currentTimeMillis() + "] This is an indefinite-duration message. " + getRandomString());

            contentView.findViewById(R.id.button_close).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    toasterController.consumeNextMessageInQueue();
                }

            });

            return contentView;
        }

    }

}
