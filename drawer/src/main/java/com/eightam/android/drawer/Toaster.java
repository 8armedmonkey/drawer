package com.eightam.android.drawer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

public class Toaster extends RelativeLayout {

    public static final long SHORT_DURATION_MILLIS = 2000;
    public static final long LONG_DURATION_MILLIS = 4000;

    protected static final int STATE_HIDDEN = 0;
    protected static final int STATE_SHOWN = 1;
    protected static final int STATE_HIDING = 2;
    protected static final int STATE_SHOWING = 3;

    private static final String BACKGROUND_COLOR = "backgroundColor";

    private int state;

    private Drawer drawer;
    private Animator animator;

    public Toaster(Context context) {
        super(context);
        initialize();
    }

    public Toaster(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public Toaster(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public void show(ShowToasterSpecification showToasterSpecification, long durationMillis) {
        setState(STATE_SHOWING);
        drawer.show(new ToasterDrawer.ShowToasterCommand(drawer, showToasterSpecification, durationMillis));
    }

    public void showSticky(ShowToasterSpecification showToasterSpecification) {
        setState(STATE_SHOWING);
        drawer.show(new ToasterDrawer.ShowStickyToasterCommand(drawer, showToasterSpecification));
    }

    public void hide(boolean animate) {
        if (state != STATE_HIDING && state != STATE_HIDDEN) {
            setState(STATE_HIDING);
            drawer.hide(new Drawer.HideCommand(drawer, animate));
        }
    }

    public boolean isToasterShown() {
        return !isToasterHidden();
    }

    public boolean isToasterHidden() {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) getLayoutParams();

        return getHeight() == 0
                || params.topMargin == -getHeight()
                || Float.compare(getAlpha(), 0.0f) == 0
                || getVisibility() != View.VISIBLE;
    }

    public boolean isToasterShowing() {
        return state == STATE_SHOWING;
    }

    public boolean isToasterHiding() {
        return state == STATE_HIDING;
    }

    public void setDrawerListener(Drawer.Listener drawerListener) {
        drawer.setListener(drawerListener);
    }

    protected synchronized void setState(int state) {
        switch (state) {
            case STATE_HIDDEN:
            case STATE_SHOWN: {
                this.state = state;
                break;
            }
            case STATE_HIDING: {
                if (this.state != STATE_HIDDEN) {
                    this.state = state;
                }
                break;
            }
            case STATE_SHOWING: {
                if (this.state != STATE_SHOWN) {
                    this.state = state;
                }
                break;
            }
        }
    }

    private void initialize() {
        drawer = new ToasterDrawer(Toaster.this);
        drawer.setListener(new ToasterDrawerListener(Toaster.this));

        post(new SetMarginTopToHide());
    }

    public static class ToasterAppearance {

        public int toasterBackgroundColor;

    }

    static class ToasterDrawer extends SimpleTopDrawer {

        public ToasterDrawer(Toaster toaster) {
            super(toaster);
        }

        @Override
        protected Animator createShowAnimator(ShowCommand showCommand) {
            Toaster toaster = (Toaster) getView();

            if (toaster.animator != null) {
                toaster.animator.cancel();
            }

            View currentContentView = toaster.getChildAt(0);
            View contentView = ((BaseShowToasterCommand) showCommand).showToasterSpecification
                    .getToasterContentProvider()
                    .provideToasterContentView(toaster.getContext(), toaster);

            ToasterAppearance toasterAppearance = ((BaseShowToasterCommand) showCommand).showToasterSpecification.getToasterAppearance();

            measureAndAddContentView(toaster, contentView);

            Animator animator = new AnimatorSet();
            List<Animator> animators = new ArrayList<>();

            Animator baseAnimator = createToasterMarginTopAnimator(toaster, contentView);
            animators.add(baseAnimator);

            if (currentContentView != null) {
                Animator fadeOutAnimator = ObjectAnimator.ofFloat(currentContentView, View.ALPHA, currentContentView.getAlpha(), 0.0f);
                animators.add(fadeOutAnimator);
            }

            Animator fadeInAnimator = ObjectAnimator.ofFloat(contentView, View.ALPHA, contentView.getAlpha(), 1.0f);
            animators.add(fadeInAnimator);

            Animator backgroundColorAnimator = createToasterBackgroundColorAnimator(toaster, toasterAppearance.toasterBackgroundColor);
            animators.add(backgroundColorAnimator);

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animators);

            Animator heightAnimator = createToasterHeightAnimator(toaster, currentContentView, contentView);
            int toasterHeight = toaster.getHeight();
            int contentViewHeight = contentView.getMeasuredHeight();

            if (toasterHeight > 0) {
                if (toasterHeight > contentViewHeight) {
                    ((AnimatorSet) animator).playSequentially(animatorSet, heightAnimator);
                } else {
                    ((AnimatorSet) animator).playSequentially(heightAnimator, animatorSet);
                }
            } else {
                ((AnimatorSet) animator).play(animatorSet);
            }

            toaster.animator = animator;

            return toaster.animator;
        }

        @Override
        protected Animator createHideAnimator(HideCommand hideCommand) {
            Toaster toaster = (Toaster) getView();

            if (toaster.animator != null) {
                toaster.animator.cancel();
            }

            View currentContentView = toaster.getChildAt(0);

            Animator animator = super.createHideAnimator(hideCommand);

            if (currentContentView != null) {
                animator.addListener(new RemoveViewAnimationListener(currentContentView));
            }

            toaster.animator = animator;

            return toaster.animator;
        }

        @Override
        protected void showViewWithoutAnimation(ShowCommand showCommand) {
            super.showViewWithoutAnimation(showCommand);

            Toaster toaster = (Toaster) getView();
            toaster.removeAllViews();

            toaster.addView(((BaseShowToasterCommand) showCommand).showToasterSpecification
                    .getToasterContentProvider()
                    .provideToasterContentView(toaster.getContext(), toaster));
        }

        void measureContentView(Toaster toaster, View contentView) {
            ViewGroup.LayoutParams params = contentView.getLayoutParams();

            int widthMeasureSpec = MeasureSpec.makeMeasureSpec(toaster.getWidth(), MeasureSpec.AT_MOST);
            int heightMeasureSpec = MeasureSpec.makeMeasureSpec(params.height, MeasureSpec.UNSPECIFIED);

            contentView.measure(widthMeasureSpec, heightMeasureSpec);
        }

        void measureAndAddContentView(Toaster toaster, View contentView) {
            measureContentView(toaster, contentView);

            contentView.setAlpha(0.0f);
            toaster.addView(contentView);
        }

        Animator createToasterMarginTopAnimator(Toaster toaster, View contentView) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toaster.getLayoutParams();

            int startMarginTop = params.topMargin;
            int endMarginTop = 0;

            if (startMarginTop == 0 && toaster.getHeight() == 0) {
                startMarginTop = -contentView.getMeasuredHeight();
            }

            float startAlpha = view.getAlpha();
            float endAlpha = 1.0f;

            Animator animator = createAnimator(startMarginTop, endMarginTop, startAlpha, endAlpha,
                    showCommand.animationStartDelayMillis, showCommand.animationDurationMillis);

            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.addListener(new CancellationTrackingAnimatorListener() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (!isCancelled()) {
                        notifyListenerOnDrawerShown();
                    }
                }

            });

            return animator;
        }

        Animator createToasterBackgroundColorAnimator(Toaster toaster, int endBackgroundColor) {
            Drawable backgroundDrawable = toaster.getBackground();
            int currentBackgroundColor = Color.TRANSPARENT;

            if (backgroundDrawable instanceof ColorDrawable) {
                currentBackgroundColor = ((ColorDrawable) backgroundDrawable).getColor();
            }

            if (currentBackgroundColor == Color.TRANSPARENT) {
                currentBackgroundColor = endBackgroundColor;
            }

            Animator animator = ObjectAnimator.ofInt(toaster, BACKGROUND_COLOR,
                    currentBackgroundColor, endBackgroundColor);

            ((ObjectAnimator) animator).setEvaluator(new ArgbEvaluator());

            return animator;
        }

        Animator createToasterHeightAnimator(Toaster toaster, View currentContentView, View contentView) {
            int toasterHeight = toaster.getHeight();
            int contentViewHeight = contentView.getMeasuredHeight();

            Animator animator = ValueAnimator.ofObject(new ViewHeightAnimator(toaster), toasterHeight, contentViewHeight);

            if (currentContentView != null) {
                animator.addListener(new RemoveViewAnimationListener(currentContentView));
            }

            return animator;
        }

        abstract static class BaseShowToasterCommand extends ShowCommand {

            ShowToasterSpecification showToasterSpecification;

            public BaseShowToasterCommand(Drawer drawer,
                                          ShowToasterSpecification showToasterSpecification) {

                super(drawer, showToasterSpecification.isAnimate());
                this.showToasterSpecification = showToasterSpecification;
            }

        }

        static class ShowToasterCommand extends BaseShowToasterCommand {

            long toasterDurationMillis;

            public ShowToasterCommand(Drawer drawer,
                                      ShowToasterSpecification showToasterSpecification,
                                      long toasterDurationMillis) {

                super(drawer, showToasterSpecification);
                this.toasterDurationMillis = toasterDurationMillis;
            }

        }

        static class ShowStickyToasterCommand extends BaseShowToasterCommand {

            public ShowStickyToasterCommand(Drawer drawer,
                                            ShowToasterSpecification showToasterSpecification) {

                super(drawer, showToasterSpecification);
            }

        }

        static class RemoveViewAnimationListener extends AnimatorListenerAdapter {

            View viewToBeRemoved;

            public RemoveViewAnimationListener(View viewToBeRemoved) {
                this.viewToBeRemoved = viewToBeRemoved;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                ViewGroup parentView = (ViewGroup) viewToBeRemoved.getParent();

                if (parentView != null) {
                    parentView.removeView(viewToBeRemoved);

                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) parentView.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    params.topMargin = 0;

                    parentView.setLayoutParams(params);
                }
            }

        }

    }

    static class ToasterDrawerListener implements Drawer.Listener {

        Toaster toaster;

        public ToasterDrawerListener(Toaster toaster) {
            this.toaster = toaster;
        }

        @Override
        public void onDrawerShown(Drawer drawer) {
            toaster.setState(STATE_SHOWN);
        }

        @Override
        public void onDrawerHidden(Drawer drawer) {
            toaster.setState(STATE_HIDDEN);
        }

    }

    class SetMarginTopToHide implements Runnable {

        @Override
        public void run() {
            MarginLayoutParams params = (MarginLayoutParams) getLayoutParams();
            params.topMargin = -getHeight();

            setLayoutParams(params);
        }

    }

}
