package com.eightam.android.drawer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

public class CancellationTrackingAnimatorListener extends AnimatorListenerAdapter {

    private boolean cancelled;

    @Override
    public void onAnimationCancel(Animator animation) {
        cancelled = true;
    }

    public boolean isCancelled() {
        return cancelled;
    }

}
