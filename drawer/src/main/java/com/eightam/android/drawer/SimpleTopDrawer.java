package com.eightam.android.drawer;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

public class SimpleTopDrawer extends Drawer {

    public SimpleTopDrawer(View view) {
        super(view);
    }

    @Override
    protected Animator createShowAnimator(ShowCommand showCommand) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();

        int startMarginTop = params.topMargin;
        int endMarginTop = 0;

        float startAlpha = view.getAlpha();
        float endAlpha = 1.0f;

        Animator animator = createAnimator(startMarginTop, endMarginTop, startAlpha, endAlpha,
                showCommand.animationStartDelayMillis, showCommand.animationDurationMillis);

        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addListener(new CancellationTrackingAnimatorListener() {

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCancelled()) {
                    notifyListenerOnDrawerShown();
                }
            }

        });

        return animator;
    }

    @Override
    protected Animator createHideAnimator(HideCommand hideCommand) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();

        int startMarginTop = params.topMargin;
        int endMarginTop = -view.getHeight();

        float startAlpha = view.getAlpha();
        float endAlpha = 0.0f;

        Animator animator = createAnimator(startMarginTop, endMarginTop, startAlpha, endAlpha,
                hideCommand.animationStartDelayMillis, hideCommand.animationDurationMillis);

        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addListener(new CancellationTrackingAnimatorListener() {

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isCancelled()) {
                    notifyListenerOnDrawerHidden();
                }
            }

        });

        return animator;
    }

    @Override
    protected void showViewWithoutAnimation(ShowCommand showCommand) {
        view.setAlpha(1.0f);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.topMargin = 0;

        view.setLayoutParams(params);

        notifyListenerOnDrawerShown();
    }

    @Override
    protected void hideViewWithoutAnimation(HideCommand hideCommand) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.topMargin = -view.getHeight();

        view.setLayoutParams(params);

        view.setAlpha(0.0f);

        notifyListenerOnDrawerHidden();
    }

    protected Animator createAnimator(int startMarginTop, int endMarginTop,
                                    float startAlpha, float endAlpha,
                                    long startDelayMillis, long durationMillis) {
        Animator marginTopAnimator = ValueAnimator.ofObject(new MarginTopAnimator(view), startMarginTop, endMarginTop)
                .setDuration(durationMillis);

        Animator alphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, startAlpha, endAlpha)
                .setDuration(durationMillis);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(marginTopAnimator, alphaAnimator);
        animatorSet.setStartDelay(startDelayMillis);

        return animatorSet;
    }

}
