package com.eightam.android.drawer;

import com.eightam.mq.Message;
import com.eightam.mq.MessageQueue;
import com.eightam.mq.MessageQueueConsumer;
import com.eightam.mq.TimeToLive;
import com.eightam.mq.TimeToLiveDefinite;
import com.eightam.mq.TimeToLiveIndefinite;

import java.lang.ref.SoftReference;

public class ToasterController {

    private static final int NOT_READY_TO_START_CONSUMING = 0;
    private static final int READY_TO_START_CONSUMING = 1;

    private int state;
    private MessageQueue messageQueue;
    private MessageQueueConsumer messageQueueConsumer;
    private Message latestConsumedMessage;

    private SoftReference<Toaster> toasterRef;

    public ToasterController() {
        this(null);
    }

    public ToasterController(Toaster toaster) {
        messageQueue = new MessageQueue();

        messageQueueConsumer = new MessageQueueConsumer(messageQueue);
        messageQueueConsumer.setListener(new MessageQueueConsumerListener());

        toasterRef = new SoftReference<>(toaster);
    }

    public void addNewShowToasterMessage(ShowToasterSpecification showToasterSpecification,
                                         long toasterDurationMillis) {
        messageQueue.offer(new PendingShowToasterMessage(showToasterSpecification, toasterDurationMillis));

        if (state == READY_TO_START_CONSUMING) {
            messageQueueConsumer.start();
        }
    }

    public void addNewShowStickyToasterMessage(ShowToasterSpecification showToasterSpecification) {
        messageQueue.offer(new PendingShowStickyToasterMessage(showToasterSpecification));

        if (state == READY_TO_START_CONSUMING) {
            messageQueueConsumer.start();
        }
    }

    public void hideToaster(boolean animate) {
        stopMessageConsumption();
        clearMessageQueue();

        Toaster toaster = getToaster();

        if (toaster != null) {
            toaster.hide(animate);
        }
    }

    public void setToaster(Toaster toaster) {
        toasterRef = new SoftReference<>(toaster);
    }

    public synchronized boolean isMessageConsumptionStarted() {
        return messageQueueConsumer.isStarted();
    }

    public synchronized void startMessageConsumption() {
        state = READY_TO_START_CONSUMING;
        messageQueueConsumer.start();
    }

    public synchronized void stopMessageConsumption() {
        state = NOT_READY_TO_START_CONSUMING;
        messageQueueConsumer.stop();
    }

    public synchronized void clearMessageQueue() {
        messageQueue.clear();
    }

    public synchronized void consumeNextMessageInQueue() {
        messageQueueConsumer.consume();
    }

    public Message getLatestConsumedMessage() {
        return latestConsumedMessage;
    }

    private Toaster getToaster() {
        return toasterRef.get();
    }

    private void showToaster(PendingShowToasterMessage message) {
        Toaster toaster = getToaster();

        if (toaster != null) {
            toaster.show(
                    message.showToasterSpecification,
                    ((TimeToLiveDefinite) message.timeToLive).getDurationMillis());
        }
    }

    private void showStickyToaster(PendingShowStickyToasterMessage message) {
        Toaster toaster = getToaster();

        if (toaster != null) {
            toaster.showSticky(message.showToasterSpecification);
        }
    }

    private void hideToasterNoStateChange(boolean animate) {
        messageQueueConsumer.stop();
        messageQueue.clear();

        Toaster toaster = getToaster();

        if (toaster != null) {
            toaster.hide(animate);
        }
    }

    abstract static class BasePendingToasterMessage implements Message {

        ShowToasterSpecification showToasterSpecification;
        TimeToLive timeToLive;

        public BasePendingToasterMessage(ShowToasterSpecification showToasterSpecification,
                                         TimeToLive timeToLive) {

            this.showToasterSpecification = showToasterSpecification;
            this.timeToLive = timeToLive;
        }

        @Override
        public String getId() {
            return showToasterSpecification.getId();
        }

        @Override
        public TimeToLive getTimeToLive() {
            return timeToLive;
        }

    }

    static class PendingShowToasterMessage extends BasePendingToasterMessage {

        public PendingShowToasterMessage(ShowToasterSpecification showToasterSpecification,
                                         long toasterDurationMillis) {
            super(showToasterSpecification, new TimeToLiveDefinite(toasterDurationMillis));
        }

    }

    static class PendingShowStickyToasterMessage extends BasePendingToasterMessage {

        public PendingShowStickyToasterMessage(ShowToasterSpecification showToasterSpecification) {
            super(showToasterSpecification, new TimeToLiveIndefinite());
        }

    }

    class MessageQueueConsumerListener implements MessageQueueConsumer.Listener {

        @Override
        public void onStart() {
        }

        @Override
        public void onConsume(Message message) {
            latestConsumedMessage = message;

            if (message instanceof PendingShowToasterMessage) {
                showToaster((PendingShowToasterMessage) message);

            } else if (message instanceof PendingShowStickyToasterMessage) {
                showStickyToaster((PendingShowStickyToasterMessage) message);

            }
        }

        @Override
        public void onNothingToConsume() {
            latestConsumedMessage = null;
            hideToasterNoStateChange(true);
        }

        @Override
        public void onStop() {
        }

    }

}
