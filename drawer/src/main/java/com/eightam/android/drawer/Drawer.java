package com.eightam.android.drawer;

import android.animation.Animator;
import android.view.View;

public abstract class Drawer {

    public static final long DEFAULT_ANIMATION_DURATION_MILLIS = 400;

    protected View view;
    protected Animator animator;
    protected ShowCommand showCommand;
    protected HideCommand hideCommand;
    protected Listener listener;

    public Drawer(View view) {
        this.view = view;
    }

    public void show(ShowCommand showCommand) {
        removeAllCallbacks();
        view.post(this.showCommand = showCommand);
    }

    public void hide(HideCommand hideCommand) {
        removeAllCallbacks();
        view.post(this.hideCommand = hideCommand);
    }

    public View getView() {
        return view;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    protected void removeAllCallbacks() {
        if (showCommand != null) {
            view.removeCallbacks(showCommand);
        }

        if (hideCommand != null) {
            view.removeCallbacks(hideCommand);
        }
    }

    protected abstract Animator createShowAnimator(ShowCommand showCommand);

    protected abstract Animator createHideAnimator(HideCommand hideCommand);

    protected abstract void showViewWithoutAnimation(ShowCommand showCommand);

    protected abstract void hideViewWithoutAnimation(HideCommand hideCommand);

    protected void showViewWithAnimation(ShowCommand showCommand) {
        if (animator != null) {
            animator.cancel();
        }

        animator = createShowAnimator(showCommand);
        animator.start();
    }

    protected void hideViewWithAnimation(HideCommand hideCommand) {
        if (animator != null) {
            animator.cancel();
        }

        animator = createHideAnimator(hideCommand);
        animator.start();
    }

    protected void notifyListenerOnDrawerShown() {
        if (listener != null) {
            listener.onDrawerShown(Drawer.this);
        }
    }

    protected void notifyListenerOnDrawerHidden() {
        if (listener != null) {
            listener.onDrawerHidden(Drawer.this);
        }
    }

    public interface Listener {

        void onDrawerShown(Drawer drawer);

        void onDrawerHidden(Drawer drawer);

    }

    public static class ShowCommand implements Runnable {

        protected Drawer drawer;
        protected boolean animate;
        protected long animationStartDelayMillis;
        protected long animationDurationMillis;

        public ShowCommand(Drawer drawer, boolean animate) {
            this(drawer, animate, 0, DEFAULT_ANIMATION_DURATION_MILLIS);
        }

        public ShowCommand(Drawer drawer, boolean animate, long animationStartDelayMillis, long animationDurationMillis) {
            this.drawer = drawer;
            this.animate = animate;
            this.animationStartDelayMillis = animationStartDelayMillis;
            this.animationDurationMillis = animationDurationMillis;
        }

        @Override
        public void run() {
            if (animate) {
                drawer.showViewWithAnimation(ShowCommand.this);
            } else {
                drawer.showViewWithoutAnimation(ShowCommand.this);
            }
        }

    }

    public static class HideCommand implements Runnable {

        protected Drawer drawer;
        protected boolean animate;
        protected long animationStartDelayMillis;
        protected long animationDurationMillis;

        public HideCommand(Drawer drawer, boolean animate) {
            this(drawer, animate, 0, DEFAULT_ANIMATION_DURATION_MILLIS);
        }

        public HideCommand(Drawer drawer, boolean animate, long animationStartDelayMillis, long animationDurationMillis) {
            this.drawer = drawer;
            this.animate = animate;
            this.animationStartDelayMillis = animationStartDelayMillis;
            this.animationDurationMillis = animationDurationMillis;
        }

        @Override
        public void run() {
            if (animate) {
                drawer.hideViewWithAnimation(HideCommand.this);
            } else {
                drawer.hideViewWithoutAnimation(HideCommand.this);
            }
        }

    }

}
