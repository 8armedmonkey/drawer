package com.eightam.android.drawer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class BottomSheet extends RelativeLayout {

    public static final long DEFAULT_DELAY_MILLIS = 400;
    public static final long ANIMATION_DURATION_MILLIS = 400;

    private Drawer drawer;

    public BottomSheet(Context context) {
        super(context);
        initialize();
    }

    public BottomSheet(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public BottomSheet(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public void show(boolean animate, long delayMillis) {
        drawer.show(new Drawer.ShowCommand(drawer, animate, delayMillis, ANIMATION_DURATION_MILLIS));
    }

    public void hide(boolean animate) {
        drawer.hide(new Drawer.HideCommand(drawer, animate));
    }

    private void initialize() {
        drawer = new SimpleBottomDrawer(BottomSheet.this);
        post(new SetMarginBottomToHide());
    }

    class SetMarginBottomToHide implements Runnable {

        @Override
        public void run() {
            MarginLayoutParams params = (MarginLayoutParams) getLayoutParams();
            params.bottomMargin = -getHeight();

            setLayoutParams(params);
        }

    }

}
