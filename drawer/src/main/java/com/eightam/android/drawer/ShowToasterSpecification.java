package com.eightam.android.drawer;

public class ShowToasterSpecification {

    private String id;
    private ToasterContentProvider toasterContentProvider;
    private boolean animate;
    private Toaster.ToasterAppearance toasterAppearance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ToasterContentProvider getToasterContentProvider() {
        return toasterContentProvider;
    }

    public void setToasterContentProvider(ToasterContentProvider toasterContentProvider) {
        this.toasterContentProvider = toasterContentProvider;
    }

    public boolean isAnimate() {
        return animate;
    }

    public void setAnimate(boolean animate) {
        this.animate = animate;
    }

    public Toaster.ToasterAppearance getToasterAppearance() {
        return toasterAppearance;
    }

    public void setToasterAppearance(Toaster.ToasterAppearance toasterAppearance) {
        this.toasterAppearance = toasterAppearance;
    }

    public static class Builder {

        String id;
        ToasterContentProvider toasterContentProvider;
        boolean animate;
        Toaster.ToasterAppearance toasterAppearance;

        public Builder id(String id) {
            this.id = id;
            return Builder.this;
        }

        public Builder toasterContentProvider(ToasterContentProvider toasterContentProvider) {
            this.toasterContentProvider = toasterContentProvider;
            return Builder.this;
        }

        public Builder animate(boolean animate) {
            this.animate = animate;
            return Builder.this;
        }

        public Builder toasterAppearance(Toaster.ToasterAppearance toasterAppearance) {
            this.toasterAppearance = toasterAppearance;
            return Builder.this;
        }

        public ShowToasterSpecification build() {
            ShowToasterSpecification showToasterSpecification = new ShowToasterSpecification();
            showToasterSpecification.id = id;
            showToasterSpecification.toasterContentProvider = toasterContentProvider;
            showToasterSpecification.animate = animate;
            showToasterSpecification.toasterAppearance = toasterAppearance;

            return showToasterSpecification;
        }

    }

}
