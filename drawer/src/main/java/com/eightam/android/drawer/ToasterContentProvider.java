package com.eightam.android.drawer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

public interface ToasterContentProvider {

    @NonNull
    View provideToasterContentView(Context context, ViewGroup parent);

}
