package com.eightam.android.drawer;

import android.animation.IntEvaluator;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

public class MarginBottomAnimator extends IntEvaluator {

    private View view;

    public MarginBottomAnimator(View view) {
        this.view = view;
    }

    @NonNull
    @Override
    public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
        int value = super.evaluate(fraction, startValue, endValue);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.bottomMargin = value;

        view.setLayoutParams(params);

        return value;
    }

}
